import parse from 'html-react-parser';

import twigTemplate from './something-cool.twig';
import data from './something-cool.yml';

const settings = {
  title: 'Components/Something Cool'
};

const SomethingCool = args =>
  parse(
    twigTemplate({
      ...args,
    })
  );
SomethingCool.args = { ...data };

export default settings;
export { SomethingCool };
