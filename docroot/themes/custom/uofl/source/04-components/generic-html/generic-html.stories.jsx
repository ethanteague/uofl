import parse from 'html-react-parser';

import twigTemplate from './generic-html.twig';
import data from './generic-html.yml';

const settings = {
  title: 'Components/Generic Html'
};

const GenericHtml = args =>
  parse(
    twigTemplate({
      ...args,
    })
  );
GenericHtml.args = { ...data };

export default settings;
export { GenericHtml };
